<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Message;

Route::get('/', function(){
    $message = Message::where('verify',1)->get();
    return view('welcome')->with('message',$message);
});
Route::resource('message','MessageController');
Route::get('message/confirm/{token}','MessageController@confirmMessage');
Route::get('message/delete/{token}','MessageController@deleteMessage');