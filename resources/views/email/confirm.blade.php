<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Confirmar mensaje</title>
</head>
<body>
    <p>Haz <a href="{{url('message/confirm/'.$message_send->token)}}">click aquí</a> para confirmar el mensaje: </p>
    <p>Haz <a href="{{url('message/delete/'.$message_send->token)}}">click aquí</a> para eliminar el mensaje: </p>
<p>{{$message_send->body}}</p>
</body>
</html>