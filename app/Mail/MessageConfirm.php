<?php

namespace App\Mail;

use App\Message;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MessageConfirm extends Mailable
{
    use Queueable, SerializesModels;
    public $message_send;
    public function __construct(Message $message_send)
    {
        $this->message_send = $message_send;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.confirm');
    }
}
