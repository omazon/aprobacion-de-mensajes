<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'body','token','verify'
    ];
    public function hasVerified(){
        $this->verify = true;
        $this->token = null;
        $this->save();
    }
    public function hasDelete(){
        $this->delete();
    }
}
